from django.conf.urls import patterns, url

from logins import views

urlpatterns = patterns('',
     url(r'^$', views.index, name='index')
)